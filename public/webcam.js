var video = document.querySelector("#videoElement");

video.setAttribute('playsinline', '');
video.setAttribute('autoplay', '');
video.setAttribute('muted', '');

async function getVideoDevices() {
    // AFAICT in Safari this only gets default devices until gUM is called :/
    return await navigator.mediaDevices.enumerateDevices().then(r => r.filter(e => e.kind === "videoinput"));
}

// let shouldFaceUser = true; //Default is the front cam

var facingMode = "user"; // Can be 'user' or 'environment' to access back or front camera (NEAT!)
var constraints = {
    audio: false,
    video: {
        facingMode: facingMode
    }
};

/* Stream it to video element */
navigator.mediaDevices.getUserMedia(constraints).then(
    function success(stream) {
        video.srcObject = stream;
    });

document.body.appendChild(video)

$(".showList").click(() => {
    if (video.classList.contains("video2")) {
        window.location.replace("http://localhost:8090/result-video-2.html");
    }
    if (video.classList.contains("video1")) {
        window.location.replace("http://localhost:8090/quizz.html");
    }
})

$(".showProfil").click(() => {
    window.location.replace("http://localhost:8090/profile.html");
})

$(".ticketScan").click(() => {
    window.location.replace("http://localhost:8090/webcam.html");
})

$(".productScan").click(() => {
    window.location.replace("http://localhost:8090/product-scan.html");
})

$(".go-back-icon").click(() => {
    window.location.replace("http://localhost:8090/product-scan.html");
})