// quiz_controller.js
//dfed8708135513083-op

function Quiz(questions) {
    this.score = 0;
    this.questions = questions;
    this.questionIndex = 0;
    this.selectedQuestion = null
}

Quiz.prototype.getQuestionIndex = function () {
    return this.questions[this.questionIndex];
};

Quiz.prototype.isEnded = function () {
    return this.questions.length === this.questionIndex;
};

Quiz.prototype.guess = function (answer) {
    if (this.getQuestionIndex().correctAnswer(answer)) {
        this.score++;
    }

    this.questionIndex++;
    drawProgressBar(this.questionIndex, this.questions.length)

    if (this.questionIndex === this.questions.length) {
        window.location = "/profile.html"
    }
    populateQuestions();
};

// question.js
function Question(text, choices, answer, answerText) {
    this.text = text;
    this.choices = choices;
    this.answer = answer;
    this.answerText = answerText;
}

Question.prototype.correctAnswer = function (choice) {
    return choice === this.answer;
};

let isSelected = false;
// app.js
function guess(id, guess) {
    var button = document.getElementById(id);
    button.onclick = function (e) {
        e.preventDefault()
        if (!isSelected) {
            showAnswer(guess)
        }
        isSelected = true;
    };
}

var questions = [
    new Question("La brosse à dent 100 bambou est-elle recyclable ?", ["Oui", "Non", "En partie"], "Non", "Malheureusement, les objets en bambous traités avec de l'huile hydrophobe (dans le cas présent, de l'huile de ricin) ne sont pas recyclables selon l'UFC que choisir."),
    new Question("Quelle est la part des canettes recyclées en France ?", ["100%", "60%", "40%", "10%", "0%"], "60%", "En France, 38% des canettes sont recyclées comme matière première secondaire (donc participent à la création de nouvelles canettes) et 22% sont valorisées en sous-couche routière "),
    new Question("Dans quel poubelle doit-on mettre les habits 100% cotton ?", ["Avec les déchets, ils ne sont pas recyclables", "Dans la poubelle de tri", "Dans des bornes de tris spécifiques"], "Dans des bornes de tris spécifiques", "Eh oui ! Il est possible de recycler le cotton notamment pour faire de l'isolant pour le batiment !"),
    new Question("Comment est composé l'emballage d'un paquet de chips ?", ["Polypropylène et aluminium", "100% Aluminium", "Papier et polypropylène", "Plastique et aluminium"], "Plastique et aluminium", "Malheureusement, les emballages de chips sont fabriqués à partir de la fusion de plastique et de papier aluminium. Un mélange rendu nécessaire par la haute teneur en graisse du produit, qui signifie qu’il deviendrait rapidement rance s’il était exposé à l’oxygène. A cause de cela, ils ne sont quasiment jamais recyclables."),
    new Question("Quel est le nom du premier sachet recyclable Haribo ?", ["Greenpack", "Doypack", "Greenbo", "Naturepack"], "Doypack", "Le premier sachet haribo entièrement recyclable est le doypack et a pour but de remplacer tous les autres depuis son lancement en Septembre 2021."),
];

var quiz = new Quiz(questions);

function populateQuestions() {
    // show question
    var questionElement = document.getElementById("question");
    questionElement.innerHTML = quiz.getQuestionIndex().text;

    // show choices
    var choices = quiz.getQuestionIndex().choices;
    var choiceElement = document.getElementById("answers-wrapper");
    choiceElement.innerHTML = "" // reset de la div

    for (var i = 0; i < choices.length; i++) {
        var question = document.createElement("button");
        let customId = "question-" + i;
        question.innerText = choices[i]
        question.className = "btn btn-outline-secondary"
        question.id = customId;
        question.type
        choiceElement.appendChild(question)
        guess(customId, choices[i]);
    }
    isSelected = false;
}

function drawProgressBar(n, total) {
    var elem = document.getElementById("bar");
    var width = n / total * 100;
    elem.style.width = width + "%";
}

function showAnswer(guess) {
    let answerElem = document.getElementById("answer-div-wrapper")
    answerElem.style.display = "flex";

    let guessData = quiz.getQuestionIndex()
    let p = document.createElement("p")
    p.className = "mx-auto"
    p.innerHTML = guessData.answerText + "<br>" + "La bonne réponse était: <b class=`fw-bold`>" + guessData.answer + "</b>";

    // <img data-src="holder.js/32x32?theme=thumb&amp;bg=007bff&amp;fg=007bff&amp;size=1" alt="32x32" class="mr-2 rounded" style="width: 32px; height: 32px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_180ae17aa07%20text%20%7B%20fill%3A%23007bff%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_180ae17aa07%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23007bff%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2211.599999904632568%22%20y%3D%2216.880000019073485%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
    // <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
    //   <strong class="d-block text-gray-dark">@username</strong>
    //   Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
    // </p>


    let btn = document.createElement("button");
    btn.type = "button"
    btn.className = "mx-auto"
    btn.onclick = (() => {
        console.log("click");
        answerElem.innerHTML = ""
        answerElem.style.display = "none";

        quiz.guess(guess);
    })
    btn.innerText = "Suivant"
    answerElem.appendChild(p)
    answerElem.appendChild(btn)

}

populateQuestions();